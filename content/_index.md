---
title: "Doni Halim"
seotitle: "Doni Halim - GNU/Linux blog, Android dan Raspberry Pi."
description: "GNU/Linux blog, tutorial seputar android dan raspberry pi."
date: 2019-07-26T22:57:50+02:00
draft: false
tags:
  - raspberrypi
  - sbc
  - linux
  - gnu/linux
  - android
  - tutorial
  - pemula
  - newbie
  - cli
resources:
- src: "*.jpg"
---
