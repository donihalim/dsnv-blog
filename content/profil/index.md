---
title: Profil - Doni Halim
seotitle: Profil - Doni Halim
date: 2019-07-02T16:11:56.000+00:00
type: about
resources:
- src: "*.jpeg"
- src: 1.jpeg
  name: cover

---
{{< photo src="profile_1.jpeg" alt="Likungan kerja Doni Halim" >}}

## **Kenalkan**

Hi, saya **Doni Halim**, berasal dari salah satu daerah kecil dibesarnya kota Bandung. Mulai menggunakan
GNU/Linux sejak **2016** sampai **sekarang**. Sehari-hari menggunakan Ubuntu 18.04 sebagai sistem
operasi utama.

***

## **DSNV**

**dsnv** hanyalah sebuah nama yang merupakan plesetan dari Doni. Tidak ada makna lain dalam nama
**dsnv** selain dari 'nama' itu sendiri.
Blog ini dibangun menggunakan [Hugo](https://gohugo.io/) *static site generator* dengan bantuan [Atom](https://atom.io/) sebagai penyunting
teks diatas sistem operasi GNU/Linux.

## **Kontak**

Jika ada yang ingin disampaikan, anda dapat menghubungi saya di alamat ini :

* Telegram: [@dsnvhlm](https://t.me/dsnvhlm)
* Twitter: [@dsnvhlm](https://twitter.com/dsnvhlm)
* Instagram: [@dsnvhlm](https://instagram.com/dsnvhlmb)
* Email: [donihalim@protonmail.com](mailto:donihalim@protonmail.com)

***

## **Credit**

* [Hugo](https://gohugo.io/) - Static site generator.
* [Hervyqa](https://hervyqa.com/) - Blog theme.
* [Atom](https://atom.io/) - Text editor.
* [Unsplash](https://unsplash.com/) - Some images.

<br style="margin: 30px">
