---
date: 2019-12-28T18:41:09.000+00:00
title: Cara Menambahkan Internal Border Pada Gnome Terminal
subtitle: ''
seotitle: Menambahkan Internal Border Pada Gnome Terminal
description: Menambahkan internal border atau padding pada gnome terminal.
slug: menambahkan-internalborder-pada-gnome-terminal
categories:
- Linux
resources:
- src: cover.jpg
  name: cover
- src: "*.jpg"

---
Internal border atau padding merupakan jarak antara teks dan batas terminal. Menambahakan internal border atau padding tidak memiliki tujuan khusus, hanya menambahkan jarak antara teks dan batas terminal saja. Jadi apa tujuan menambahkan internal border atau padding pada terminal ? Untuk sebagian orang termasuk saya hal tersebut bertujuan untuk membuat kita lebih _nyaman_ ketika bekerja didalam terminal.

***

Buat file dengan nama `gtk.css` pada direktori `~/.config/gtk-3.0/`

    $ touch ~/.config/gtk-3.0/gtk.css

Lalu tambahkan baris berikut pada file `gtk.css` :

    vte-terminal {
        padding: 8px;
    }

Simpan dan tutup terminal, lalu buka kembali aplikasi gnome terminal.

## **Penutup**

Sekian untuk artikel kali ini, semoga dapat membatu dan bermanfaat bagi Anda.