---
draft: true
date: 2020-01-09T11:01:48Z
title: "Shortcut Keyboard Bash"
subtitle: "Buat aktivitas Anda dalam terminal linux lebih efisien dengan shortcut keyboard bash."
seotitle: "Shortcut Keyboard Terminal Linux"
description : "Macam-macam shortcut keyboard bash yang wajib Anda ketahui."
slug: ""
categories:
- linux
resources:
- src: "cover.jpg"
  name: "cover"
- src: "*.jpg"
---

**Sampurasun, pada kesempatan kali ini saya akan membagikan beberapa shortcut keyboard yang
terdapat dalam shell bash. Hal ini dapat membantu Anda menjadi lebih produktif saat bekerja dengan
terminal linux.**

***

**Bash** merupakan default [shell](https://en.m.wikipedia.org/wiki/Shell_(computing)) pada kebanyakan sistem
operasi GNU/Linux dari mulai Ubuntu dan Debian sampai Redhat dan Fedora.

Ada banyak sekali shortcut keyboard didalam bash, namun saat ini saya hanya akan membagikan beberapa saja.
Saya akan mengelompokan sesuai dengan penggunaan nya.

## **Mengelola Proses**
Pintasan-pintasan ini memungkinkan Anda untuk mengelola proses yang sedang berjalan.

- **`CTRL+C`** - Menghentikan(kill) proses yang sedang bejalan di latar
    depan(foreground).

- **`CTRL+Z`** - Men-suspend proses yang sedang berjalan ke latar belakang(background). Untuk mengembalikan proses tersebut ke latar
    depan(foreground), gunakan perintah: `fg nama_proses`.

- **`CTRL+D`** - Keluar dari sesi yang sedang berjalan. Shortcut ini mirip dengan perintah
    `exit` pada terminal.

## **Mengelola Layar**
Pintasan-pintasan ini memungkinkan Anda untuk mengatur apa yang tampil pada layar.

- **`CTRL+L`** - Mengosongkan layar. Shorcut ini mirip dengan perintah `clear` pada terminal.

- **`CTRL+S`** - Menghentikan sementara output dari perintah yang dijalankan pada layar.

- **`CTRL+Q`** - Menampilkan kembali output dari perintah yang dijalankan setelah menekan shorcut `CTRL+S`.

## **Memindahkan Cursor**
Memindahkan cursor secara cepat sembari mengetikan perintah.

- **`CTRL+A`** atau **`Home`** - Memindahkan cursor ke awal baris.

- **`CTRL+E`** atau **`End`** - Memindahkan cursor ke akhir baris.

- **`ALT+B`** - Pindah ke kiri(mundur) satu kalimat.

- **`CTRL+B`** - Pindah ke kiri(mundur) satu karakter atau huruf.

- **`ALT+F`** - Pindah ke kanan(maju) satu kalimat.

- **`CTRL+F`** - Pindah ke kanan(maju) satu karakter atau huruf.


# **Besok Lagi, Cape**
