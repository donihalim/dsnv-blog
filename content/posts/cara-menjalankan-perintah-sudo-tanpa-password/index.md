---
draft: true
date: 2020-01-08T04:24:23Z
title: "Cara Menjalankan Perintah Sudo Tanpa Password"
subtitle: "Menjalankan perintah sudo tanpa memasukan password."
seotitle: "Cara Menjalankan Perintah Sudo Tanpa Password"
description : "Artikel ini menjelaskan bagaimana cara menjalankan perintah sudo tanpa perlu memasukan
password."
slug: ""
categories:
- linux
resources:
- src: "cover.jpg"
  name: "cover"
- src: "*.jpg"
---

**Sampurasun, pada kesempatan kali ini saya akan menjelaskan bagaimana cara menjalankan perintah-perintah sudo
tertentu atau seluruh perintah sudo tanpa perlu memasukan password.**

***

Pada saat anda menjalankan perintah dengan sudo, maka anda akan disuguhkan sebuah prompt yang meminta anda
untuk memasukan password sudo. Secara default, setelah anda memasukan password sudo maka sesi tersebut akan
bertahan beberapa menit, artinya anda tidak perlu lagi memasukan password saat kembali menjalankan sudo.

Sebagian orang menganggap hal tersebut 'rumit' karena harus memasukan ulang password sudo setiap
beberapa menit sekali. Terutama bagi mereka yang setiap waktunya bekerja dengan terminal linux untuk
menjalankan perintah tertentu yang memerlukan akses ***superuser***.

Di linux, anda dimungkinkan untuk melakukan modifikasi pada file konfigurasi sudo yang berada pada
`/etc/sudoers`.

>Jika anda dalam linux server, anda harus ekstra hati-hati dalam melakukan hal ini, terutama jika anda
mengaktifkan **SSH** pada server linux.

**Baiklah, kita mulai.**

Pertama, lakukanlah pencadangan pada file konfigurasi sudo kedalam direktori ***home*** dengan perintah
berikut :

```
$ sudo cp /etc/sudoers ~/sudoers.bak
```

## **Menjalankan semua perintah sudo tanpa password**
Jalankan perintah berikut untuk memodifikasi file `/etc/sudoers` :

```
$ sudo visudo
```

Perintah tersebut akan membuka teks editor default pada sistem operasi anda(**Nano** pada Ubuntu) untuk
memodifikasi file `sudoers`. Lalu tambahkan baris berikut pada file konfigurasi :

```
user ALL=(ALL) NOPASSWD:ALL
```

Ubah **user** dengan nama user anda.

Setelah itu, simpan modifikasi yang telah dibuat lalu keluar dari teks editor. Tutup dan buka kembali
terminal, maka saat ini anda tidak perlu lagi memasukan password saat menggunakan perintah sudo.

Saya sendiri kurang merekomendasikan mebuat sudo tidak memerlukan password sama sekali. Karena hal
tersebut dapat membahayakan sistem jika kita lalai. Namun jangan khawatir, anda juga dapat membuat hanya
perintah-perintah tertentu saja yang tidak memerlukan password.

## **Menjalankan perintah sudo tertentu tanpa password**
Anda juga dapat memodifikasi sudo agar hanya perintah-perintah tertentu saja yang tidak meminta anda
untuk memasukan password.

Misalnya anda berada didalam sistem operasi Ubuntu dan ingin membuat perintah `sudo apt update` tidak
meminta anda untuk memasukan password, maka anda dapat memodifikasi sudo sebagai berikut.

Buka kebali file konfigurasi sudo :

```
$ sudo visudo
```

Lalu tambahkan baris berikut :

```
user ALL=(ALL) NOPASSWD:/usr/bin/apt update
```
Sama seperti sebelumnya, ubah **user** dengan nama user anda. Simpan hasil modifikasi, lalu tutup dan buka
kembali terminal baru.

Maka saat ini seharusnya anda dapat menjalankan perintah `sudo apt update` tanpa perlu memasukan
password.

## **Penutup**
Baiklah, sekian tutorial singkat dari saya, semoga dapat membantu dan menambah wawasan anda mengenai Linux.
Jika dirasa ada yang kurang jelas, atau anda memiliki pertanyaan, silahkan berkomentar melalui kolom
komentar dibawah ini. Terima kasih.

***

**Referensi** :
- [Sudo](https://www.sudo.ws/)
- [Linux Handbook](https://linuxhandbook.com/sudo-without-password/)
