---
draft: true
date: {{ .Date }}
title: "{{ replace .Name "-" " " | title }}"
subtitle: ""
seotitle: ""
description : ""
slug: ""
categories:
- android
- catatan
- desain
- desktop
- dokumen
- impress
- inkscape
- linux
- perkakas
- reaspberrypi
resources:
- src: "cover.jpeg"
  name: "cover"
- src: "*.jpeg"
---

Bismillahirrohmanirrohim.

***

<div>{{< youtube xxxxxxxxxxx >}}</div>

{{< photo src=".jpeg" alt="" >}}

{{< photoset max="2" >}}
  {{< photo src=".jpeg" alt="" >}}
  {{< photo src=".jpeg" alt="" >}}
{{</ photoset >}}

{{< photoset max="3" >}}
  {{< photo src=".jpeg" alt="" >}}
  {{< photo src=".jpeg" alt="" >}}
  {{< photo src=".jpeg" alt="" >}}
{{</ photoset >}}

> Lorem ipsum doloret amet.

***
